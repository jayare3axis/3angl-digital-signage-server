<?php

namespace Triangl\Tests;

use Triangl\WebTestCase;
use Triangl\DigitalSignageApplication;

/**
 * Functional test for Triangl Digital Signage.
 */
class DigitalSignageTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new DigitalSignageApplication( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
}
