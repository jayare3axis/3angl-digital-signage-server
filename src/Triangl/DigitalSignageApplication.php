<?php

namespace Triangl;

/**
 * Digital Signage application.
 */
class DigitalSignageApplication extends BackendApplication {
    /**
     * Overriden.
     */
    protected function init() {
        parent::init();
        
        $this->register( new DigitalSignage() );
    }
    
    /**
     * Overriden.
     */
    public function isDebug() {
        return parent::isDebug();
    }
}
