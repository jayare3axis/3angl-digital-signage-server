<?php

namespace Triangl\Entity;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Filtres set-top box by it's type.
 */
class SettopBoxTypeFilter extends SQLFilter {
    /**
     * Implemented.
     */
    public function addFilterConstraint(ClassMetaData $targetEntity, $targetTableAlias)
    {
        if ( $targetEntity->getName() != 'Triangl\Entity\DigitalSignage\SettopBox' ) {
            return '';
        }
        return $targetTableAlias.'.stb_type_id = ' . $this->getParameter('stb_type_id');
    }
}
