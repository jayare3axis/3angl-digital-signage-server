<?php

namespace Triangl\Entity\DigitalSignage;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\OrderTrait;
use Triangl\Entity\DigitalSignage\SettopBox;
use Triangl\Entity\DigitalSignage\Playlist;

/**
 * Association between set-top box and playlist.
 * @Entity @Table(name="stb_playlists")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 */
class SettopBoxPlaylistAssociation {
    use PrimaryIdTrait;
    use OrderTrait;
    
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\DigitalSignage\SettopBox", inversedBy="stb_playlist_associations")
     * @JoinColumn(name="stb_id", referencedColumnName="id")
     */
    private $stb;
 
    /**
     * @ManyToOne(targetEntity="Triangl\Entity\DigitalSignage\Playlist", inversedBy="stb_playlist_associations")
     * @JoinColumn(name="playlist_id", referencedColumnName="id")
     */
    private $playlist;
    
    /**
     * Set set-top box.
     * @param \Triangl\Entity\DigitalSignage\SettopBox $stb
     * @return \Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation this
     */
    public function setSettopBox(SettopBox $stb = null) {
        $this->stb = $stb; 
        return $this;
    }
 
    /**
     * Get set-top box
     * @return \Triangl\Entity\DigitalSignage\SettopBox
     */
    public function getSettopBox() {
        return $this->stb;
    }
 
    /**
     * Set playlist.
     * @param \Triangl\Entity\DigitalSignage\Playlist $playlist
     * @return \Triangl\Entity\DigitalSignage\Playlist
     */
    public function setPlaylist(Playlist $playlist = null)
    {
        $this->playlist = $playlist; 
        return $this;
    }
 
    /**
     * Get playlist
     * @return \Triangl\Entity\DigitalSignage\Playlist
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }
}
