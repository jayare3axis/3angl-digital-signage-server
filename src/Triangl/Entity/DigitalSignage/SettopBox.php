<?php

namespace Triangl\Entity\DigitalSignage;

use Doctrine\Common\Collections\ArrayCollection;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\BelongsToDomainTrait;
use Triangl\Entity\MacAddressTrait;
use Triangl\Entity\BelongsToDomainInterface;
use Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociationTrait;

/**
 * Set-top box entity.
 * @Entity @Table(name="stbs")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class SettopBox implements BelongsToDomainInterface {
    use PrimaryIdTrait;    
    use NameTrait;
    use BelongsToDomainTrait;
    use MacAddressTrait;
    use SettopBoxPlaylistAssociationTrait;
    
    /**
     * @ManyToOne(targetEntity="\Triangl\Entity\DigitalSignage\SettopBoxType")
     * @JoinColumn(name="stb_type_id", referencedColumnName="id")
     **/
    private $stb_type;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation", mappedBy="stb")
     * @OrderBy({"ord" = "ASC"})
     */
    private $stb_playlist_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->stb_playlist_associations = new ArrayCollection();
    }
    
    /**
     * Gets set top box type.
     * @return \Triangl\Entity\DigitalSignage\SettopBoxType
     */
    public function getStbType() {
        return $this->stb_type;
    }
}
