<?php

namespace Triangl\Entity\DigitalSignage;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\DigitalSignage\SettopBox;

/**
 * Set-top box command entity.
 * @Entity @Table(name="stb_commands")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class SettopBoxCommand {
    use PrimaryIdTrait; 
    
    /**
     * @ManyToOne(targetEntity="\Triangl\Entity\DigitalSignage\SettopBox")
     * @JoinColumn(name="stb_id", referencedColumnName="id")
     **/
    private $stb;
    
    /** @Column(type="string") **/
    private $command;
    
    /** @Column(type="datetime") **/
    private $date_time;
    
    /**
     * Default constructor.
     */
    public function __construct()
    {
        $this->date_time = new \DateTime(); 
    }
    
    /**
     * Gets set-top box.
     * @param \Triangl\Entity\DigitalSignage\SettopBox $stb     
     */
    public function setStb(SettopBox $stb) {
        $this->stb = $stb;
    }
    
    /**
     * Sets set-top box.
     * @return \Triangl\Entity\DigitalSignage\SettopBox
     */
    public function getStb() {        
        return $this->stb;
    }
    
    /**
     * Gets the command.
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Sets the command.
     * @param string $command
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }
    
    /**
     * Gets the date time.
     * @return DateTime
     */
    public function getDateTime()
    {
        return $this->date_time;
    }

    /**
     * Sets the command.
     * @param DateTime $dateTime
     */
    public function setDateTime(\DateTime $dateTime)
    {
        $this->date_time = $dateTime;
    }
}
