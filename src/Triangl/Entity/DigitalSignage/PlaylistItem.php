<?php

namespace Triangl\Entity\DigitalSignage;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\OrderTrait;
use Triangl\Entity\FileTrait;
use Triangl\Entity\DigitalSignage\Playlist;

/**
 * Playlist items entity.
 * @Entity @Table(name="playlist_items")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class PlaylistItem {
    use PrimaryIdTrait;    
    use NameTrait;
    use OrderTrait;
    use FileTrait;
    
    /**
     * @ManyToOne(targetEntity="\Triangl\Entity\DigitalSignage\Playlist")
     * @JoinColumn(name="playlist_id", referencedColumnName="id")
     **/
    private $playlist;
    
    /**
     * Sets playlist.
     * @param \Triangl\Entity\DigitalSignage\Playlist $playlist
     * @return \Triangl\Entity\DigitalSignage\PlaylistItem this
     */
    public function setPlaylist(Playlist $playlist = null) {
        $this->playlist = $playlist;
        return $this;
    }
    
    /**
     * Gets playlist.
     * @return \Triangl\Entity\DigitalSignage\Playlist
     */
    public function getPlaylist() {
        return $this->playlist;
    }
}
