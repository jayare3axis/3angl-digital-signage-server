<?php

namespace Triangl\Entity\DigitalSignage;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\BelongsToDomainTrait;
use Triangl\Entity\BelongsToDomainInterface;

/**
 * Set-top box type entity.
 * @Entity @Table(name="stb_types")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class SettopBoxType implements BelongsToDomainInterface {
    use PrimaryIdTrait;    
    use NameTrait;
    use BelongsToDomainTrait;
    
    /** @Column(type="string") **/
    protected $layout;
    
    /** @Column(type="string") **/
    protected $city;
    
    /** @Column(type="integer") **/
    protected $refresh_rate;
    
    /**
     * Gets the layout.
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Sets the layout.
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }
    
    /**
     * Gets the city.
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city.
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
    
    /**
     * Gets the refresh rate.
     * @return int
     */
    public function getRefreshRate()
    {
        return $this->refresh_rate;
    }

    /**
     * Sets the interval.
     * @param int $refreshRate
     */
    public function setRefreshRate($refreshRate)
    {
        $this->refresh_rate = $refreshRate;
    }
}
