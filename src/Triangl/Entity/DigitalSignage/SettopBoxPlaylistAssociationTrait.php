<?php

namespace Triangl\Entity\DigitalSignage;

use Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation;

/**
 * Entity with property of type stb-playlist association
 */
trait SettopBoxPlaylistAssociationTrait {    
    /**
     * Add set-top box playlist association.
     * @param \Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation $stbPlaylistAssociation
     * @return \Triangl\Entity\DigitalSignage\SettopBox this
     */
    public function addSettopBoxPlaylistAssociation(SettopBoxPlaylistAssociation $stbPlaylistAssociation)
    {
        $this->stb_playlist_associations[] = $stbPlaylistAssociation; 
        return $this;
    }
 
    /**
     * Remove user_recipe_associations
     * @param \Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation $stbPlaylistAssociation
     */
    public function removeSettopBoxPlaylistAssociation(SettopBoxPlaylistAssociation $stbPlaylistAssociation)
    {
        $this->stb_playlist_associations->removeElement($stbPlaylistAssociation);
    }
     
    /**
     * Gets associations.
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettopBoxPlaylistAssociations()
    {
        return $this->stb_playlist_associations;
    }
}
