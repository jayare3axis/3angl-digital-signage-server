<?php

namespace Triangl\Entity\DigitalSignage;

use Triangl\Entity\PrimaryIdTrait;
use Triangl\Entity\NameTrait;
use Triangl\Entity\BelongsToDomainTrait;
use Triangl\Entity\BelongsToDomainInterface;
use Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociationTrait;

/**
 * Playlists entity.
 * @Entity @Table(name="playlists")
 * @Entity(repositoryClass="Triangl\Entity\EntityRepository")
 **/
class Playlist implements BelongsToDomainInterface {
    use PrimaryIdTrait;    
    use NameTrait;
    use BelongsToDomainTrait;
    use SettopBoxPlaylistAssociationTrait;
    
    /** @Column(type="string") **/
    private $anim;
        
    /** @Column(type="smallint") **/
    private $delay;
    
    /**
     * @OneToMany(targetEntity="\Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation", mappedBy="playlist")
     */
    private $stb_playlist_associations;
    
    /**
     * Default constructor.
     */
    public function __construct() {
        $this->stb_playlist_associations = new ArrayCollection();
    }
    
    /**
     * Gets the anim.
     * @return string
     */
    public function getAnim()
    {
        return $this->anim;
    }

    /**
     * Sets the name.
     * @param string $anim
     */
    public function setAnim($anim)
    {
        $this->anim = $anim;
    }
    
    /**
     * Gets the delay
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * Sets the name.
     * @param string $delay
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;
    }
}
