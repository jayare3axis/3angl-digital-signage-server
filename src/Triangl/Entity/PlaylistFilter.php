<?php

namespace Triangl\Entity;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Filtres playlist items by playlist.
 */
class PlaylistFilter extends SQLFilter {
    /**
     * Implemented.
     */
    public function addFilterConstraint(ClassMetaData $targetEntity, $targetTableAlias)
    {
        if ( $targetEntity->getName() != 'Triangl\Entity\DigitalSignage\PlaylistItem' ) {
            return '';
        }
        return $targetTableAlias.'.playlist_id = ' . $this->getParameter('playlist_id');
    }
}
