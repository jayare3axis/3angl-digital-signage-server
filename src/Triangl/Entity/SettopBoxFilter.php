<?php

namespace Triangl\Entity;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Filtrs playlists by it's set-top box.
 */
class SettopBoxFilter extends SQLFilter {
    /**
     * Implemented.
     */
    public function addFilterConstraint(ClassMetaData $targetEntity, $targetTableAlias)
    {
        if ( $targetEntity->getName() != 'Triangl\Entity\DigitalSignage\Playlist' ) {
            return '';
        }
        return $targetTableAlias.'.stb_id = ' . $this->getParameter('stb_id');
    }
}
