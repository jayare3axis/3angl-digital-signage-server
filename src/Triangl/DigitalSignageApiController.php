<?php

namespace Triangl;

use Triangl\Entity\DigitalSignage\Playlist;
use Triangl\Entity\DigitalSignage\PlaylistItem;
use Triangl\Entity\DigitalSignage\SettopBoxCommand;

/*
 * Controller for navigating between System menu.
 */
class DigitalSignageApiController extends ApiController {
    /**
     *  Provides data for Digital Signage client.
     *  @param string $mac
     */
    public function dataAction($mac) {
        $result = array();
        
        try {
            $em = $this->app['db.orm.em'];
            $stb = $this->getStb($mac);

            $stbType = $stb->getStbType();

            if ($stbType == null) {
                throw new \Exception("Set-top box with mac address $mac was not properly configured.");
            }
            
            array_push( $result, array(
                'class' => 'StbType',
                'config' => array(
                    "city" => $stbType->getCity(),
                    "refreshRate" => $stbType->getRefreshRate(),
                    "activity" => $stbType->getLayout(),
                    "macAddress" => $stb->getMac()
                )
            ) );
            
            foreach ( $stb->getSettopBoxPlaylistAssociations() as $association ) {
                $playlist = $association->getPlaylist();
                array_push( $result, $this->loadPlaylist($playlist) );
            }
        }
        catch (\Exception $e) {
            $result = $this->formatException($e);
            $this->app->log($e);
        }
        
        return $this->app->json($result);
    }
    
    /**
     * Inserts new command for given set-top box.
     * @param int $id
     * @param string $command
     */
    public function insertCommand($id, $command) {
        $result = array();
        
        try {
            $em = $this->app['db.orm.em'];
            $stb = $em->getRepository('\Triangl\Entity\DigitalSignage\SettopBox')->find($id);
            
            $instance = new SettopBoxCommand();
            $instance->setStb($stb);
            $instance->setCommand($command);
            
            $em->persist($instance);
            $em->flush();
        } 
        catch (\Exception $e) {
            $result = $this->formatException($e);
            $this->app->log($e);
        }
        
        return $this->app->json($result); 
    }
    
    /**
     * Processes command (if any) for given set-top box.
     * @param string $mac
     */
    public function execCommand($mac) {
        $result = array();
        $command = "";
        
        try {
            $em = $this->app['db.orm.em'];
            
            $stb = $this->getStb($mac);
            
            // Find oldest command for given set-top box.
            $qb = $em->createQueryBuilder();
            $qb->select('e')
                ->from( '\Triangl\Entity\DigitalSignage\SettopBoxCommand', 'e')
                ->where('e.stb = :stb')
                ->orderBy('e.date_time', 'ASC')
                ->setParameter('stb', $stb);
            $query = $qb->getQuery();            
            $r = $query->getResult();
            
            // If there is command, add it to result and remove.
            if ( count($r) > 0 ) {
                $instance = $r[0];
                $command = $instance->getCommand();
                $em->remove($instance);
                $em->flush();
            }
            
            $result['command'] = $command;
        } 
        catch (\Exception $e) {
            $result = $this->formatException($e);
            $this->app->log($e);
        }
        
        return $this->app->json($result); 
    }
    
    /**
     * Gets set-top box instance by it's mac address.
     * @param string $mac
     */
    private function getStb($mac) {
        $em = $this->app['db.orm.em'];
        
        $stb = $em->getRepository('\Triangl\Entity\DigitalSignage\SettopBox')->findOneBy( array('mac' => $mac) );
        
        if ($stb == null) {
            throw new \Exception("Set-top box with mac address $mac not found.");
        }
        
        return $stb;
    }
    
    /**
     * Loads playlist.
     * @param Triangl\Entity\DigitalSignage\Playlist $playlist
     */
    private function loadPlaylist(Playlist $playlist) {
        return array(
            'class' => 'Playlist',
            'config' => array(
                'anim' => $playlist->getAnim(),
                'delay' => $playlist->getDelay(),
                'items' => $this->loadPlaylistItems($playlist)
            )
        );
    }
    
    /**
     * Loads playlist items.
     * @param Triangl\Entity\DigitalSignage\Playlist $playlist
     */
    private function loadPlaylistItems(Playlist $playlist) {
        $em = $this->app['db.orm.em'];
        
        $items = array();
        
        $filter = $em->getFilters()->enable('playlist');
        $filter->setParameter( 'playlist_id', $playlist->getId() );
        
        foreach ( $em->getRepository('\Triangl\Entity\DigitalSignage\PlaylistItem')->findAll() as $playlistItem ) {
            array_push( $items, $this->loadPlaylistItem($playlistItem) );
        }
        
        return $items;
    }
    
    /**
     * Loads playlist item.
     * @param Triangl\Entity\DigitalSignage\PlaylistItem $playlistItem
     */
    private function loadPlaylistItem(PlaylistItem $playlistItem) {
        // TO - DO determine video item
        return array(
            'type' => 'ImageItem',
            'url' => $this->app['backend.upload.fm']->getUploadPath() . '/' . $playlistItem->getFile()->getName()
        );
    }
}
