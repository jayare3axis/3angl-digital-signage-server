<?php

namespace Triangl\Provider;

use Silex\ServiceProviderInterface;

use Triangl\DigitalSignageApiController;

/**
 * Service that handles value of Html page title tag.
 */
class DigitalSignageApiServiceProvider implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function register(\Silex\Application $app)
    {
        // Controllers.
        $app['ds.api.controller'] = $app->share(function() use ($app) {
            return new DigitalSignageApiController($app);
        });
        
        // Routes.
        $app->get('api/data/{mac}', 'ds.api.controller:dataAction')
            ->bind('stb_data');
        $app->post('api/command/insert/{command}/{id}', 'ds.api.controller:insertCommand')
            ->bind('stb_insert_command');
        $app->post('api/command/exec/{mac}', 'ds.api.controller:execCommand')
            ->bind('stb_exec_command');
    }

    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app)
    {
    }
}
