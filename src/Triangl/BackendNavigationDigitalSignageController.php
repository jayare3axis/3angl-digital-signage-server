<?php

namespace Triangl;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\Navigation\MenuItemLeaf;

/*
 * Controller for navigating between System menu.
 */
class BackendNavigationDigitalSignageController extends Controller {
    /**
     *  Set-top boxes action.
     */
    public function stbsAction() {
        $className1 = '\Triangl\Entity\DigitalSignage\SettopBoxType';
        $className2 = '\Triangl\Entity\DigitalSignage\SettopBox';
        
        $stbTypeId = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $stbTypes = $repository->findAll();
        if ( count($stbTypes) > 0 ) {
            $stbTypeId = $stbTypes[0]->getId();
        }
        
        // Create set-top box menu.
        $stbMenu = $this->app['db.orm.grid.menu']->createMenu($className2);
        $menu = new MenuItemComposite("Set-top box", null);
        $item = new MenuItemLeaf('Sync', 'stb_insert_command');
        $item->pushArg('id', '-1');
        $item->pushArg('command', 'sync');
        $item->addData('btn-type', 'info');
        $item->addClass('tool-sync');
        $menu->pushChild($item);
        $item = new MenuItemLeaf('Reboot', 'stb_insert_command');
        $item->pushArg('id', '-1');
        $item->pushArg('command', 'reboot');
        $item->addData('btn-type', 'info');
        $item->addClass('tool-reboot');
        $menu->pushChild($item);
        $item = new MenuItemLeaf('Update', 'stb_insert_command');
        $item->pushArg('id', '-1');
        $item->pushArg('command', 'update');
        $item->addData('btn-type', 'info');
        $item->addClass('tool-update');
        $menu->pushChild($item);
        $stbMenu->pushChild($menu);  
        
        $this->app['navigation.backend']->selectByRoute('backend_stbs');
        return $this->app['twig']->render( 'backend_content_categorized_grid_edit.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'grid_menu1' => $this->app['db.orm.grid.menu']->createMenu($className1),
            'grid_menu2' => $stbMenu,
            'orderBy' => 'id:ASC',
            'filter' => 'stbType',
            'filterParam' => 'stb_type_id',
            'filterValue' => $stbTypeId,
            'filterProp' => 'stb_type'
        ) );
    }    
    
    /**
     *  Playlists action.
     */
    public function playlistsAction() {
        $className1 = '\Triangl\Entity\DigitalSignage\Playlist';
        $className2 = '\Triangl\Entity\DigitalSignage\PlaylistItem';
        
        $playlistId = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $playlists = $repository->findAll();
        if ( count($playlists) > 0 ) {
            $playlistId = $playlists[0]->getId();
        }
        
        $itemsMenu = $this->app['db.orm.grid.menu']->createMenu($className2);
        $menu = new MenuItemComposite("Order", null);
        $item = new MenuItemLeaf(
            '', 'change_order', 'arrow-up'
        );
        $item->pushArg('className', $className2);
        $item->pushArg('method', 'up');
        $item->addData('btn-type', 'info');
        $item->addClass('tool-move-up');
        $menu->pushChild($item);
        $item = new MenuItemLeaf(
            '', 'change_order', 'arrow-down'
        );
        $item->pushArg('className', $className2);
        $item->pushArg('method', 'down');
        $item->addData('btn-type', 'info');        
        $item->addClass('tool-move-down');
        $menu->pushChild($item);
        $itemsMenu->pushChild($menu);
        
        $this->app['navigation.backend']->selectByRoute('backend_playlists');
        return $this->app['twig']->render( 'backend_content_categorized_grid_edit.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'grid_menu1' => $this->app['db.orm.grid.menu']->createMenu($className1),
            'grid_menu2' => $itemsMenu,
            'orderBy' => 'ord:ASC',
            'filter' => 'playlist',
            'filterParam' => 'playlist_id',
            'filterValue' => $playlistId,
            'filterProp' => 'playlist'
        ) );
    }
    
    /**
     * Playlist assignement action.
     */
    public function playlistAssignementAction() {
        $className1 = '\Triangl\Entity\DigitalSignage\SettopBox';
        $className2 = '\Triangl\Entity\DigitalSignage\Playlist';
        $associationClass = '\Triangl\Entity\DigitalSignage\SettopBoxPlaylistAssociation';
        
        // Get set-top box id.
        $stb = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $stbs = $repository->findAll();
        if ( count($stbs) > 0 ) {
            $stb = $stbs[0];
        }
        
        // Menu for adding items.
        $addMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Add', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-add');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'stb');
        $item->pushArg('targetProperty', 'playlist');
        $item->pushArg('method', 'add');
        $menu->pushChild($item);
        $addMenu->pushChild($menu);
        
        // Menu for removing items.
        $remMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Remove', 'entity_edit_association_class'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-remove');
        $item->pushArg('className', $className2);
        $item->pushArg('associationClass', $associationClass);
        $item->pushArg('property', 'stb');
        $item->pushArg('targetProperty', 'playlist');
        $item->pushArg('method', 'remove');        
        $menu->pushChild($item);
        $remMenu->pushChild($menu);
        
        // Menu for ordering items.
        // TO - DO determine order
        $menu = new MenuItemComposite("Order", null);
        $item = new MenuItemLeaf(
            '', 'change_order', 'arrow-up'
        );
        $item->pushArg('className', $associationClass);
        $item->pushArg('method', 'up');
        $item->addData('btn-type', 'info');
        $item->addClass('tool-move-up');
        $menu->pushChild($item);
        $item = new MenuItemLeaf(
            '', 'change_order', 'arrow-down'
        );
        $item->pushArg('className', $associationClass);
        $item->pushArg('method', 'down');
        $item->addData('btn-type', 'info');        
        $item->addClass('tool-move-down');
        $menu->pushChild($item);
        $remMenu->pushChild($menu);
        
        $this->app['navigation.backend']->selectByRoute('backend_playlist_assignement');
        return $this->app['twig']->render( 'backend_content_edit_association.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'associationClass' => $associationClass,
            'grid_menu1' => $addMenu,
            'grid_menu2' => $remMenu,
            'property' => 'stb',
            'parentId' => ($stb != null) ? $stb->getId() : null
        ) );
        
        // Previous implementation where was playlists couldn't replicate.
        /*$className1 = '\Triangl\Entity\DigitalSignage\SettopBox';
        $className2 = '\Triangl\Entity\DigitalSignage\Playlist';
        
        $stbId = null;
        $repository = $this->app['db.orm.em']->getRepository($className1);
        $stbs = $repository->findAll();
        if ( count($stbs) > 0 ) {
            $stbId = $stbs[0]->getId();
        }
                
        $addMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Add', 'entity_edit_association'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-add');
        $item->pushArg('className', '\Triangl\Entity\DigitalSignage\Playlist');
        $item->pushArg('filter', 'stb');
        $item->pushArg('property', 'stb');
        $item->pushArg('method', 'add');
        $menu->pushChild($item);
        $addMenu->pushChild($menu); 
        
        $remMenu = new MenuBuilder();
        $menu = new MenuItemComposite("Assign", null);
        $item = new MenuItemLeaf(
            'Remove', 'entity_edit_association'
        );
        $item->addData('btn-type', 'info');
        $item->addClass('tool-remove');
        $item->pushArg('className', '\Triangl\Entity\DigitalSignage\Playlist');
        $item->pushArg('filter', 'stb');
        $item->pushArg('property', 'stb');
        $item->pushArg('method', 'remove');        
        $menu->pushChild($item);
        $remMenu->pushChild($menu);
        
        $this->app['navigation_backend']->selectByRoute('backend_playlist_assignement');
        return $this->app['twig']->render( 'backend_content_edit_relation.html.twig', array(
            'className1' => $className1,
            'className2' => $className2,
            'grid_menu1' => $addMenu,
            'grid_menu2' => $remMenu,
            'filter' => 'stb',
            'filterParam' => 'stb_id',
            'filterValue' => $stbId,
            'filterProp' => 'stb'
        ) );*/
    }
}
