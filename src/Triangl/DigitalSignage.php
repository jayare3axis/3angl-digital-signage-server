<?php

namespace Triangl;

use Silex\ServiceProviderInterface;

use Triangl\Component\Navigation\MenuBuilder;
use Triangl\Component\Navigation\MenuItemLeaf;
use Triangl\Component\Navigation\MenuItemComposite;
use Triangl\Component\BuildGridEvent;
use Triangl\Component\BuildBackendMenuEvent;

use Triangl\Provider\DigitalSignageApiServiceProvider;

/*
 * Triangl digital signage module.
 */
class DigitalSignage implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        // Digital Signage configurations
        $app['name'] = "Digital Signage";
        $app['bootstrap.theme'] = 'spacelab';             
        if ( $app->isDebug() ) {
            $app['asset.path'] = 'http://localhost/assets';
            $app['bootstrap.path'] = 'http://localhost/assets/bootstrap-3.2.0-dist';   
            $app['jq.file.upload.path'] = 'http://localhost/assets/jQuery-File-Upload-9.8.0';
            $app['backend.upload.path'] = 'http://localhost/3angl-ds/web/uploads';
            $app['db'] = array(
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'digital_signage',
                'user' => 'root',
                'password' => null,
            );
        }
        else {
            $app['asset.path'] = 'http://beta.ipan.cz/web/assets';
            $app['bootstrap.path'] = 'http://beta.ipan.cz/web/assets/bootstrap-3.2.0-dist';
            $app['jq.file.upload.path'] = 'http://beta.ipan.cz/web/assets/jQuery-File-Upload-9.8.0';
            $app['backend.upload.path'] = 'http://beta.ipan.cz/web/uploads';
            $app['db'] = array(
                'driver' => 'pdo_mysql',
                'host' => 'dbsrv3.web4ce.cz',
                'dbname' => 'beta_ipan_cz',
                'user' => 'u13507',
                'password' => 'tzf5xc12cq',
            );
        }
        $app['backend.upload.dir'] = $app['path'] . '/../web/uploads';
        
        // Grid configuration.
        $app['dispatcher']->addListener('backend.build.grid', function (BuildGridEvent $event) {
            if ( $event->getClassName() == '\Triangl\Entity\DigitalSignage\SettopBox' ) {
                $event->setProperties( array('name', 'mac', 'stb_playlist_associations') );
            }
            else if ( $event->getClassName() == '\Triangl\Entity\DigitalSignage\SettopBoxType' ) {
                $event->setProperties( array('name', 'layout', 'city') );
            }
            else if ( $event->getClassName() == '\Triangl\Entity\DigitalSignage\Playlist' ) {
                $event->setProperties( array('name', 'anim', 'delay') );
            }
            else if ( $event->getClassName() == '\Triangl\Entity\DigitalSignage\PlaylistItem' ) {
                $event->setProperties( array('name', 'file') );
            }
        });
        
        // Backend navigation.
        $app['dispatcher']->addListener('backend.build.navigation', function (BuildBackendMenuEvent $event) {
            $this->buildBackendMenu( $event->getBuilder(), $event->getContext() );
        });
        
        // Register template folder.
        $app['twig.loader.filesystem']->prependPath(__DIR__ . "/../../views");
        
        // Register entities.
        $app['triangl.entities']->addNamespace( array(
            'type' => 'annotation',
            'path' => __DIR__ . '/Entity/DigitalSignage',
            'namespace' => 'Triangl\Entity\DigitalSignage'
        ) );
        
        // Register filters.
        $app['triangl.entities']->addFilter('stbType', '\Triangl\Entity\SettopBoxTypeFilter');
        $app['triangl.entities']->addFilter('playlist', '\Triangl\Entity\PlaylistFilter');
        $app['triangl.entities']->addFilter('stb', '\Triangl\Entity\SettopBoxFilter');
        
        // Register services.
        $app->register( new DigitalSignageApiServiceProvider() );
        
        // Forward index request to admin.
        $app->get('/', function () use ($app) {
            return $app->redirect( $app->path('backend') );
        });
        
        // Controllers.
        $app['backend.ds.navigation.controller'] = $app->share(function() use ($app) {
            return new BackendNavigationDigitalSignageController($app);
        });
        
        // Routes
        $app->get($app["backend.url"] . "/stbs", "backend.ds.navigation.controller:stbsAction")
            ->bind('backend_stbs');
        $app->get($app["backend.url"] . "/playlists", "backend.ds.navigation.controller:playlistsAction")
            ->bind('backend_playlists');
        $app->get($app["backend.url"] . "/playlistsAssignement", "backend.ds.navigation.controller:playlistAssignementAction")
            ->bind('backend_playlist_assignement');
    }
    
    /**
     * Builds backend menu.
     * @param \Triangl\Component\Navigation\MenuBuilder $builder
     * @param \Silex\Application $app
     */
    private function buildBackendMenu(MenuBuilder $builder, \Silex\Application $app) {
        $user = $app["security.user.instance"];
        $action = $app['triangl.security']->getActionByAlias('view');
                
        $menu = new MenuItemComposite("Content", null, "list-alt");
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\DigitalSignage\SettopBox', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Set-top boxes", 'backend_stbs'
            ) );
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\DigitalSignage\Playlist', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Playlists", 'backend_playlists'
            ) );            
        }
        
        if ( $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\DigitalSignage\SettopBox', $action)
             && $app['triangl.security']->isUserAllowed($user, '\Triangl\Entity\DigitalSignage\Playlist', $action) ) {
            $menu->pushChild( new MenuItemLeaf(
                "Playlist Assignement", 'backend_playlist_assignement'
            ) );
        }
                
        $builder->pushChild($menu, true);
    }
}
