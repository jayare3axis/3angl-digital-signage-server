<?php

require_once __DIR__ . '/vendor/autoload.php';

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Triangl\DigitalSignageApplication;

$app = new DigitalSignageApplication( __DIR__ . '/var', array('test' => true) );
$app->boot();

return ConsoleRunner::createHelperSet($app['db.orm.em']);
